const express = require('express');
const app = express();
const server = require('http').createServer(app);

const io = require('./startup/startup.io').initialize(server);

process.on('unhandledRejection', (reason, p) => {
    console.log("Unhandled Rejection at: Promise ", p, " reason: ", reason);
});

process.on('uncaughtException', function (exception) {
    console.log(exception);
});

require('./startup/startup.logger')();
require('./startup/startup.config')(); 
require('./startup/startup.db')();
require('./startup/startup.socket').socketStart(io);
require('./startup/startup.routes')(app);

require('./startup/startup.cron')();

module.exports = server;
