const winston = require('winston');
const mongoose = require('mongoose');
const nconf = require('nconf');
const fs = require('fs');
const gridfs = require('mongoose-gridfs');

let Attachment;

module.exports = function () {
  let db;
  if (process.env.NODE_ENV === "cloud") db = nconf.get('cloudDb');
  else db = nconf.get('db');
  mongoose.connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  }) //replicaSet: 'rs0'
    .then(() => {
      winston.info(`Connected to ${db}...`);
    });
}