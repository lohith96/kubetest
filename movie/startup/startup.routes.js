const express = require('express');
const cors = require('cors');
const fetch = require('node-fetch')
// const { Kafka } = require('kafkajs')
// const kafka = new Kafka({
//   clientId: 'movie-service',
//   brokers: ["kafka-service:9092"]
// })

const corsOptions = function (req, callback) {
  var corsOptions = {
    origin: "*",
    exposedHeaders: 'x-auth-token',
  };
  callback(null, corsOptions);
}

module.exports = function (app) {

  app.use((req, res, next) => {
    if (req.method !== "OPTIONS") {
      console.log(req.method, ":", req.originalUrl);
    }
    next();
  });

  app.use(express.json({ limit: '50mb' }));
  app.use(cors(corsOptions));
  app.use(express.static(__dirname));


  app.get('/', (req, res) => {
    console.log('request has hit');
    return res.status(200).send({ msg: "ALL IS GOOD IN MOVIE COMPONENT" })
  });

  app.get('/hitUser/', async (req, res) => {
    try {
      console.log('hitting user deployment');

      // const producer = kafka.producer()

      // await producer.connect()
      // await producer.send({
      //   topic: 'test-topic',
      //   messages: [
      //     { value: 'Hello KafkaJS user!' },
      //   ],
      // })

      // await producer.disconnect()

      // return res.status(200).send("DONE :)");

      const response = await fetch('http://user-service:3000/');
      if (response) {
        console.log(response);
        return res.status(200).send(await response.json());
      } else {
        return res.status(200).send('Could not hit user deployment');
      }
    } catch (error) {
      console.log(error);
      return res.status(500).send("ERROR :(")
    }
  });
}