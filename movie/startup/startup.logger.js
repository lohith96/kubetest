let winston = require('winston');
const path = require('path');
const { createLogger, transports, format } = winston;
const util = require('util');

module.exports = function() {
  try {
    let logger = createLogger({
      level:'silly',
      format: format.combine(
        winston.format.colorize(),
        winston.format.timestamp(),
        winston.format.simple(),
        winston.format.printf( info => {
          return info.stack
          ? `${info.level}: ${ util.inspect(info.stack, {
            depth: 2,
            compact:true,
            colors: true,
          })}`
          : `${info.level}: ${ util.inspect(info.message, {
            depth: 2,
            compact:true,
            colors: true,
          })}`
        })
      ),
      transports: [new transports.Console()],
    });

    if (process.env.NODE_ENV === "production") {
      logger.add(
        new winston.transports.File({
          filename: path.join(__dirname, '../logs/combined.log'),
          level: "silly"
        })
      );
      logger.add(
        new winston.transports.File({
          filename: path.join(__dirname, '../logs/error.log'),
          level: "info"
        })
      );
      winston.info("IN PRODUCTION MODE");
    }

    winston.add(logger);
  } catch (error) {
    console.log(error);    
  }

};
