var sio = require('socket.io');
var io = null;
var mappings = [];
exports.getIo = function () {
  return io;
};

exports.getMappings = function() {
  return mappings;
}

exports.initialize = function(server) {
  return io = sio(server, {
    path: '/socket'
  });
  // return io = sio(server);
};
