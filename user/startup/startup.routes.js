const express = require('express');
const cors = require('cors');

// const { Kafka } = require('kafkajs')
// const kafka = new Kafka({
//   clientId: 'user-service',
//   brokers: ["kafka-service:9092"]
// })

const corsOptions = function (req, callback) {
  var corsOptions = {
    origin: "*",
    exposedHeaders: 'x-auth-token',
  };
  callback(null, corsOptions);
}

module.exports = function (app) {

  app.use((req, res, next) => {
    if (req.method !== "OPTIONS") {
      console.log(req.method, ":", req.originalUrl);
    }
    next();
  });

  app.use(express.json({ limit: '50mb' }));
  app.use(cors(corsOptions));
  app.use(express.static(__dirname));
  
  app.get('/', async (req, res) => {
    console.log('request has hit');

    // const consumer = kafka.consumer({ groupId: 'test-group' })

    // await consumer.connect()
    // await consumer.subscribe({ topic: 'test-topic', fromBeginning: true })
    
    // await consumer.run({
    //   eachMessage: async ({ topic, partition, message }) => {
    //     console.log({
    //       value: message.value.toString(),
    //     })
    //   },
    // })
    

    return res.status(200).send({msg: "ALL IS GOOD IN USER 3 COMPONENT"})
  })
  
}