const winston = require('winston');
const CronJob = require('cron').CronJob;

module.exports = function() {
    winston.info(`Starting CRON Job`);
    new CronJob('0 */4 * * *', function() {
        try {
            winston.info(`Performing CRON Job once every 4 hours. Time is ${new Date()}`);    
        } catch (error) {
            console.log(error);
        }
    }, null, true, 'Asia/Kolkata');
}