const server = require('./app');
const nconf = require('nconf');
const winston = require('winston');

server.listen(nconf.get("port"), () => {
    winston.info(`Listening on port: ${nconf.get("port")}`);
});
